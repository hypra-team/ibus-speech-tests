#!/bin/bash
# LICENSE: GPLv3+
#
# Tests ibus-speech throughout the stack.  This tries to test the setup as a whole, as close as
# possible as a user would.  To do so, we need a fully functional installation, and then we:
# * Set a couple of ibus-speech options the way we expect them;
# * Create a virtual PulseAudio device and redirect input and output through it;
# * Enable the speech ibus engine;
# * Play a pre-recorded sample through the redirected output;
# * Meanwhile, read terminal input, hoping for ibus input;
# * Check the received input against the expected one;
# * Clean the environment up.
#
# This should be as close as it gets to what actually happens when a user triggers ibus-speech
# manually, as it goes through the exact same bits.  The downside is that it requires a running
# and fully functional set up in order to run the test.

set -e

SELF_DIR="$(dirname "$(command -v "$0")")"

# Cleans the environment up (basically, does the reverse from setup()).
# It is used as a trap handler.
cleanup() {
  local status=$?
  if test $status -ne 0; then
    echo "FAILURE: \`$BASH_COMMAND\` exited with status $status" >&2
  else
    echo "Cleaning up..."
  fi

  ibus engine "$orig_ibus_engine"
  spd-say -C || :  # make sure we don't hear any leftover message
  sleep 1
  pactl set-default-source "$orig_default_source"
  pactl set-default-sink "$orig_default_sink"

  # find the module to unload
  module_id=$(LANG=C pactl list sinks | sed -n '/^\tName: '"$sink"'$/,/^$/{s/\tOwner Module: //p}') || :
  if test -n "$module_id"; then
    pacmd unload-module "$module_id"
  else
    echo "Failed to determine module to unload, $sink leaked!" >&2
  fi

  gsettings set fr.hypra.IBusSpeech state-feedback "$orig_state_feedback"
  gsettings set fr.hypra.IBusSpeech speech-feedback "$orig_speech_feedback"
  gsettings set fr.hypra.IBusSpeech automatic-punctuation "$orig_automatic_punctuation"
}

# Sets the environment up and activates ibus-speech
setup() {
  orig_state_feedback=$(gsettings get fr.hypra.IBusSpeech state-feedback)
  orig_speech_feedback=$(gsettings get fr.hypra.IBusSpeech speech-feedback)
  orig_automatic_punctuation=$(gsettings get fr.hypra.IBusSpeech automatic-punctuation)

  orig_default_sink=$(LANG=C pactl info | grep -Po '(?<=^Default Sink: ).*$')
  orig_default_source=$(LANG=C pactl info | grep -Po '(?<=^Default Source: ).*$')

  sink=ibus-speech-virtual-sink
  source="$sink.monitor"

  orig_ibus_engine=$(ibus engine)

  # reset on exit, now we got all state saved
  trap cleanup EXIT INT TERM QUIT

  pacmd load-module module-null-sink sink_name="$sink"

  gsettings set fr.hypra.IBusSpeech state-feedback false
  gsettings set fr.hypra.IBusSpeech speech-feedback 'none'
  gsettings set fr.hypra.IBusSpeech automatic-punctuation true

  pactl set-default-source "$source"
  pactl set-default-sink "$sink"
  sleep 1

  # we ignore return code from setting the engine ATM, because it most likely stems from failure
  # to set the XKB layout, because ibus-speech does not provide any layout information.
  # We use the error output instead, that should be empty in our case.
  err=$(ibus engine speech:fr-FR 2>&1 1>/dev/null) || test -z "$err"
  sleep 5  # let ibus-speech "boot up"
}

echo "Setting up environment..."
setup

echo "Recognizing sample..."

# play sample and get data from ibus
ibus_output=$("$SELF_DIR"/ibus-input.py "$SELF_DIR"/samples/sample-fr1.ogg)

echo "data from ibus: $ibus_output"
expected_output="$(cat "$SELF_DIR"/samples/sample-fr1.txt)"
if test "$ibus_output" = "$expected_output"; then
  echo OK
else
  echo "**FAILED**"
  echo "EXPECTED: $expected_output"
  echo "ACTUAL:   $ibus_output"
  exit 1
fi >&2
