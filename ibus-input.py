#!/usr/bin/env python3
# LICENSE: GPLv3+
#
# Tiny helper program to play an audio sample and wait for a single text input.  This is tailored to
# receive one single input from an input method: it reports the entered text and exists as soon as
# *any* text is entered in the entry.  For an input method, this means one single commit.
# The programs exists with an error if the window is closed, or after a 20 seconds timeout.

import sys
import gi
gi.require_version('Gdk', '3.0')
gi.require_version('Gtk', '3.0')
gi.require_version('Gst', '1.0')
from gi.repository import Gdk
from gi.repository import Gtk
from gi.repository import GLib
from gi.repository import Gio
from gi.repository import Gst


def error(msg, prefix='Error: '):
    print((prefix or '') + msg, file=sys.stderr)
    sys.exit(1)


class Window(Gtk.Window):
    def __init__(self, sample_file):
        super().__init__(type_hint=Gdk.WindowTypeHint.DIALOG, resizable=False)

        # audio playback
        self._playbin = Gst.ElementFactory.make('playbin', 'playbin')
        if not self._playbin:
            error("cannot create GStreamer pipeline")
        else:
            self._playbin.set_property('uri', sample_file.get_uri())
            bus = self._playbin.get_bus()
            bus.add_signal_watch()
            bus.connect('message::error', self._on_gst_error)

        # UI
        self.connect('destroy', Gtk.main_quit)
        self.connect('map', self._on_map)

        box = Gtk.Box(visible=True, orientation=Gtk.Orientation.VERTICAL)
        self.add(box)

        box.add(Gtk.Label(label="Waiting for input, do NOT switch window!",
                          visible=True))

        entry = Gtk.Entry(visible=True, width_chars=32)
        entry.connect('notify::text', self._entry_notify_text)
        entry.connect('focus-in-event', self._entry_focus)
        box.add(entry)

        entry.grab_focus()

        self.present()

    def _on_gst_error(self, bus, msg):
        err, dbg = msg.parse_error()
        error("%s: %s" % (msg.src.get_name(), err.message))

    def _on_map(self, widget):
        GLib.timeout_add_seconds(20, self._timeout)

    def _timeout(self):
        error("timeout reached")
        return False

    def _playback(self):
        if self._playbin.set_state(Gst.State.PLAYING) == Gst.StateChangeReturn.FAILURE:
            error("failed to play sample")
        return False

    def _entry_focus(self, entry, *args):
        GLib.idle_add(self._playback)
        return False

    def _entry_notify_text(self, entry, *args):
        text = entry.get_text().strip()
        if text:
            print(text)
            self.destroy()
            sys.exit(0)


if __name__ == '__main__':
    Gtk.init(sys.argv)
    Gst.init(sys.argv)

    if len(sys.argv) != 2:
        error("USAGE: %s FILE" % sys.argv[0], prefix=None)
    else:
        w = Window(Gio.File.new_for_commandline_arg(sys.argv[1]))
        Gtk.main()

    sys.exit(1)
